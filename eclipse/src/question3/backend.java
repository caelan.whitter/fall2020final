package question3;

import java.util.Random;

public class backend {
	private double money;
	Random random = new Random();
//sets money
	public backend(double money) {
		this.money=money;
	}
	
	//returns money
	public double getMoney()
	{
		return this.money;
	}
	//adds money
	public void addMoney(double amount)
	{
		this.money= this.money+amount;
	}
	//minuses money
	public void minusMoney(double amount)
	{
		this.money = this.money-amount;
	}
	//compares choice to computers choice
	public boolean bet(String choice)
	{
		int randNum = random.nextInt(2);
		String compChoice;

		if (randNum == 0)
		{
			compChoice = "heads";
		}
		else
		{
			compChoice = "tails";
		}
		
		if(choice.equals("heads") && compChoice.equals("heads"))
		{
			return true;
		}
		else if(choice.equals("tails") && compChoice.equals("tails"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
