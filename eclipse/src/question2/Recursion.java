package question2;

import java.util.Arrays;

public class Recursion {
	private static int index=0;
	private static int number=0;

	public static void main(String[] args) 
	{
		String[] words = {"aqua","quad","hello","hi","qaid","quad","qaid"};
		System.out.println(recursiveCount(words,2));
	}
	//will check array to see if it meets all requirement by calling other method
	public static int recursiveCount(String[] words, int n)
	{
	
		int result = 0;

		if (match(words[words.length-1],index,n))
		{
			number++;
		}
		index++;

		if(words.length == 1)
		{
			return number;
		}
		else
		{
			return recursiveCount(Arrays.copyOf(words, words.length-1),n);
		}
	}
	
	// is the if statement for the recursive
	public static boolean match(String word, int arrayInd, int n)
	{
		if(word.contains("q"))
		{
			if(arrayInd % 2 == 0 && arrayInd<=n)
			{
				System.out.println(word);
			return true;
			}
		}
		return false;
	}

}
