package question3;


import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class frontend extends Application {
	private backend game = new backend(100);
	
	private Text errorM;
	private Text total;
//sets up the UI
	public void start(Stage stage) throws Exception {
		Group root = new Group(); 
	
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
	

		//associate scene to stage and show
		stage.setTitle("Heads or Tails"); 
		stage.setScene(scene); 
		
		stage.show(); 
		
		//buttons
		HBox things = new HBox();
		TextField money = new TextField();
		Button heads = new Button("Heads");
		Button tails = new Button("Tails");
		things.getChildren().addAll(money,heads,tails);
		
		VBox everything = new VBox();
		this.errorM = new Text();
		this.total = new Text();
		everything.getChildren().addAll(things,errorM,total);
		
		

		
		//root
		root.getChildren().add(everything);
		
        heads.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                dealWithHeads(money.getText());
            } 
       }
       );
        
        tails.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                dealWithTails(money.getText());
            } 
       }
       );
		
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
    // if user presses head , deal with it in the backend
    public void dealWithHeads(String money)
    {
      	if(money.equals(""))
    	{
    		this.errorM.setText("Enter amount");

    	}
      	else
      	{
    	if(this.game.getMoney()<Double.parseDouble(money))
    	{
    		this.errorM.setText("Not enough money");
    	}
    	else
    	{
    	boolean bet = this.game.bet("heads");
    	if(bet)
    	{
    		this.game.addMoney(Double.parseDouble(money));
    		this.errorM.setText("You won "+money);
    		this.total.setText("You have "+String.valueOf(this.game.getMoney())+" left");


    	}
    	else
    	{
    		this.game.minusMoney(Double.parseDouble(money));
    		this.errorM.setText("You lost "+money);
    		this.total.setText("You have "+String.valueOf(this.game.getMoney())+" left");
    	}
    	}
      	}
    }
    // if user presses tails , deal with it in the backend

    public void dealWithTails(String money)
    {
    	if(money.equals(""))
    	{
    		this.errorM.setText("Enter amount");

    	}
    	else
    	{
    	if(this.game.getMoney()<Double.parseDouble(money))
    	{
    		this.errorM.setText("Not enough money");
    	}
    	else
    	{
    	boolean bet = this.game.bet("tails");
    	if(bet)
    	{
    		this.game.addMoney(Double.parseDouble(money));
    		this.errorM.setText("You won "+money);
    		this.total.setText("You have "+String.valueOf(this.game.getMoney())+" left");
    	}
    	else
    	{
    		this.game.minusMoney(Double.parseDouble(money));
    		this.errorM.setText("You lost "+money);
    		this.total.setText("You have "+String.valueOf(this.game.getMoney())+" left");
    	

    	}
    	}
    	}
    }


}
